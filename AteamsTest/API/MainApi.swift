//
//  MainApi.swift
//  AteamsTest
//
//  Created by Admin on 20.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

protocol MainApi {
    
    func sendRequest(withUrl url: String, completion: ServerResult?)
    
}

extension MainApi {
    
    func sendRequest(withUrl url: String, completion: ServerResult?) {
        
        guard let url = URL.init(string: url)
            else {
                completion?(.Error(message: "Incorrect url"))
                return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, responce, error) in
            
            do {
                guard let data = data, let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] else {
                    completion?(.Error(message: "error trying to convert data to JSON"))
                    return
                }
                completion?(.Success(response: json))
            } catch  {
                completion?(.Error(message: "error trying to convert data to JSON"))
                return
            }
            }.resume()
    }
    
    func sendRequest(withUrl url: String, completion: ServerArrayResult?) {
        
        guard let url = URL.init(string: url)
            else {
                completion?(.Error(message: "Incorrect url"))
                return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, responce, error) in
            
            do {
                guard let data = data, let json = try JSONSerialization.jsonObject(with: data, options: []) as? [[String: AnyObject]] else {
                    completion?(.Error(message: "error trying to convert data to JSON"))
                    return
                }
                completion?(.Success(response: json))
            } catch  {
                completion?(.Error(message: "error trying to convert data to JSON"))
                return
            }
            }.resume()
    }
    
}
