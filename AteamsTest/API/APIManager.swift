//
//  APIManager.swift
//  AteamsTest
//
//  Created by Admin on 19.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct APIManager : MainApi {
    
    private init() {  }
    private let session = URLSession.shared
    
    static let shared = APIManager()
    
    func fetchItem(withUrl url: String, completion: ServerResult?) {
        sendRequest(withUrl: url, completion: completion)
    }
    
    func fetchItems(withUrl url: String, completion: ServerArrayResult?) {
        sendRequest(withUrl: url, completion: completion)
    }
}
