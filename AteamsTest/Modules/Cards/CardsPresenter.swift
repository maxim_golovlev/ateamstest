//
//  CardsPresenter.swift
//  AteamsTest
//
//  Created by Admin on 19.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import PromiseKit

protocol CardsPresenterProtocol: class {
    weak var view:CardsViewProtocol? { get set }
    func fetchData(postNumber: Int, commentNumber: Int)
    func fetchPost(number: Int, sender: BaseView, completion: @escaping (Post) -> ())
    func fetchComment(number: Int, sender: BaseView, completion: @escaping (Comment) -> ())
}

class CardsPresenter: CardsPresenterProtocol {
    
    weak var view:CardsViewProtocol?
    
    init(view:CardsViewProtocol) {
        self.view = view
    }
    
    func fetchData(postNumber: Int, commentNumber: Int) {
        
        var _post: Post?
        var _comment: Comment?
        var _users: [User]?
        var _photo: Photo?
        var _todo: Todo?
        
        view?.startLoading()
        
        firstly {
            CardsManager.shared.fetchPost(number: postNumber)
        }
        .then { (post) -> Promise<Comment> in
            _post = post
            return CardsManager.shared.fetchComment(number: commentNumber)
        }
        .then { (comment) -> Promise<[User]> in
            _comment = comment
            return CardsManager.shared.fetchTopUsers(count: 5)
        }
        .then { (users) -> Promise<Photo> in
            _users = users
            return CardsManager.shared.fetchPhoto(number: 3)
        }
        .then { (photo) -> Promise<Todo> in
            _photo = photo
            let number = arc4random_uniform(200)
            return CardsManager.shared.fetchTodo(number: number)
        }
        .then { (todo) -> Void in
            _todo = todo
            
            self.view?.dataDidUploaded(post: _post, comment: _comment, users: _users, photo: _photo, todo: _todo)
            
        }
        .always {
            self.view?.stopLoading()
        }
        .catch { (error) in
            if case let ResponseError.withMessage(msg) = error {
                self.view?.showAlert(title: nil, message: msg)
            }
        }
        
    }
    
 func fetchPost(number: Int, sender: BaseView, completion: @escaping (Post) -> ()) {
        
        sender.startLoading()
        
        CardsManager.shared.fetchPost(number: number)
        .then { (post) -> Void in
            completion(post)
        }
        .always {
            sender.stopLoading()
        }
        .catch { (error) in
            if case let ResponseError.withMessage(msg) = error {
                sender.showAlert(title: nil, message: msg)
            }
        }
    }
    
 func fetchComment(number: Int, sender: BaseView, completion: @escaping (Comment) -> ()) {
        
        sender.startLoading()
        
        CardsManager.shared.fetchComment(number: number)
            .then { (comment) -> Void in
                completion(comment)
            }
            .always {
                sender.stopLoading()
            }
            .catch { (error) in
                if case let ResponseError.withMessage(msg) = error {
                    sender.showAlert(title: nil, message: msg)
                }
        }
    }
}
