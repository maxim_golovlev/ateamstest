//
//  CardsViewController.swift
//  AteamsTest
//
//  Created by Admin on 19.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

protocol CardsViewProtocol: class, BaseView {
    func dataDidUploaded(post: Post?, comment: Comment?, users: [User]?, photo: Photo?, todo: Todo?)
}

class CardsViewController: UIViewController {

    lazy var presenter:CardsPresenterProtocol = CardsPresenter(view: self)
    
    var post: Post?
    var comment: Comment?
    var users: [User]?
    var photo: Photo?
    var todo: Todo?
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableDirector = TableDirector.init(tableView: tableView)
            tableView.tableFooterView = UIView()
            tableView.allowsSelection = false
        }
    }
    private var tableDirector: TableDirector!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.fetchData(postNumber: 1, commentNumber: 1)
    }
}

extension CardsViewController: CardsViewProtocol {
    
    func dataDidUploaded(post: Post?, comment: Comment?, users: [User]?, photo: Photo?, todo: Todo?) {
        
        self.post = post
        self.comment = comment
        self.users = users
        self.photo = photo
        self.todo = todo
        
        tableDirector.clear()
        //post
        let postSection = TableSection.init(headerTitle: "Posts", footerTitle: nil)
        
        let updatePostAction = TableRowAction<EditableCell>(.custom(AppActions.updatePostCell)) { (options) in
            if let cell = options.cell {
                if let text = cell.textField.text, let number = Int(text) {
                    self.presenter.fetchPost(number: number, sender: self, completion: { (post) in
                        self.dataDidUploaded(post: post, comment: self.comment, users: self.users, photo: self.photo, todo: self.todo)
                    })
                }
            }
        }
        
        let postCell = TableRow<EditableCell>.init(item: (title: post?.title, subtitle: post?.body, type: .Post), actions: [updatePostAction])
        
        postSection.append(row: postCell)
        //comment
        let commentsSection = TableSection.init(headerTitle: "Comments", footerTitle: nil)
        
        let updateCommentAction = TableRowAction<EditableCell>(.custom(AppActions.updateCommentCell)) { (options) in
            if let cell = options.cell {
                if let text = cell.textField.text, let number = Int(text) {
                    self.presenter.fetchComment(number: number, sender: self, completion: { (comment) in
                        self.dataDidUploaded(post: self.post, comment: comment, users: self.users, photo: self.photo, todo: self.todo)
                    })
                }
            }
        }
        
        let commentCell = TableRow<EditableCell>.init(item: (title: comment?.name, subtitle: comment?.body, type: .Comment), actions: [updateCommentAction])
        
        commentsSection.append(row: commentCell)
        //users
        let usersSection = TableSection.init(headerTitle: "Users", footerTitle: nil)
        if let users = users {
            for user in users {
                let userCell = TableRow<UserCell>.init(item: (number: "\(user.id)", name: user.name))
                usersSection.append(row: userCell)
            }
        }
        //photo
        let photoSection = TableSection.init(headerTitle: "Photos", footerTitle: nil)
        let photoCell = TableRow<PhotoCell>.init(item: photo?.url)
        photoSection.append(row: photoCell)
        //todo
        let todoSection = TableSection.init(headerTitle: "Todos", footerTitle: nil)
        let todoCell = TableRow<TodoCell>.init(item: (title: todo?.title, completed: todo?.completed))
        todoSection.append(row: todoCell)
        
        tableDirector.append(sections: [postSection, commentsSection, usersSection, photoSection, todoSection])
        
        tableDirector.reload()
    }
}
