//
//  ContactsViewController.swift
//  AteamsTest
//
//  Created by Admin on 19.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Contacts
import TableKit

class ContactsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableDirector = TableDirector.init(tableView: tableView)
            tableView.tableFooterView = UIView()
            tableView.allowsSelection = false
        }
    }
    
    private var tableDirector: TableDirector!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fetchContacts { contacts in
            DispatchQueue.main.async {
                self.updateTableView(contacts: contacts)
            }
        }
    }
    
    func updateTableView(contacts: [CNContact]) {
        
        tableDirector.clear()
        
        let formatter = CNContactFormatter()
        formatter.style = .fullName
        
        let contactsSection = TableSection.init(headerTitle: "Contacts", footerTitle: nil)
        for (i, contact) in contacts.enumerated() {
            let name = formatter.string(from: contact) ?? "???"
            let userRow = TableRow<UserCell>.init(item: (number: "\(i + 1)", name: name))
            contactsSection.append(row: userRow)
        }
        
        tableDirector.append(section: contactsSection)
        
        tableDirector.reload()
    }

    func fetchContacts(completion: @escaping ([CNContact]) -> ()){
        
        let status = CNContactStore.authorizationStatus(for: .contacts)
        if status == .denied || status == .restricted {
            presentSettingsActionSheet()
            return
        }
        
        // open it
        
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { granted, error in
            guard granted else {
                DispatchQueue.main.async {
                    self.presentSettingsActionSheet()
                }
                return
            }
            
            // get the contacts
            
            var contacts = [CNContact]()
            let request = CNContactFetchRequest(keysToFetch: [CNContactIdentifierKey as NSString, CNContactFormatter.descriptorForRequiredKeys(for: .fullName)])
            do {
                try store.enumerateContacts(with: request) { contact, stop in
                    contacts.append(contact)
                    completion(contacts)
                }
            } catch {
                print(error)
            }
        }
    }
    
    func presentSettingsActionSheet() {
        let alert = UIAlertController(title: "Permission to Contacts", message: "This app needs access to contacts in order to ...", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Go to Settings", style: .default) { _ in
            let url = URL(string: UIApplicationOpenSettingsURLString)!
            UIApplication.shared.open(url)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(alert, animated: true)
    }

}
