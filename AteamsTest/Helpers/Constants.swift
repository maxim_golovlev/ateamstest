
import Foundation
import UIKit

struct AppScreen {
    static let scale = UIScreen.main.scale
    static let height = UIScreen.main.bounds.size.height
    static let width = UIScreen.main.bounds.size.width
    static let pixel = 1 / UIScreen.main.scale
    static let navbarHeight = 64
    static let tabbarHeight = 49
}

enum ResponseError: Error {
    case withMessage(String?)
}

struct AppActions {
    static let updatePostCell = "updatePostCell"
    static let updateCommentCell = "updateCommentCell"
}
