//
//  CardsManager.swift
//  AteamsTest
//
//  Created by Admin on 19.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import PromiseKit

class CardsManager: NSObject {
    
    struct APIKeys {
        static func post(n: Int) -> String { return "https://jsonplaceholder.typicode.com/posts/\(n)" }
        static func comment(n: Int) -> String { return "https://jsonplaceholder.typicode.com/comments/\(n)" }
        static func users() -> String { return "https://jsonplaceholder.typicode.com/users" }
        static func photo(n: Int) -> String { return "https://jsonplaceholder.typicode.com/photos/\(n)" }
        static func todo(n: UInt32) -> String { return "https://jsonplaceholder.typicode.com/todos/\(n)" }
    }
    
    private override init() { }
    
    static let shared = CardsManager()
    
    func fetchPost(number: Int) -> Promise<Post> {
        
        let urlString = APIKeys.post(n: number)
            
            return Promise.init(resolvers: { (fullfill, reject) in
                
                APIManager.shared.fetchItem(withUrl: urlString, completion: { response in
                   
                    switch response {
                        case let .Success(response: json):
                            if let post = Post.initfromDict(dict: json) {
                                fullfill(post)
                            } else {
                                reject(ResponseError.withMessage("No such post"))
                            }
                        case let .Error(message: msg):
                            reject(ResponseError.withMessage(msg))
                        }
                })
            })
    }
    
    func fetchComment(number: Int) -> Promise<Comment> {
        
        let urlString = APIKeys.comment(n: number)
        
        return Promise.init(resolvers: { (fullfill, reject) in
            
            APIManager.shared.fetchItem(withUrl: urlString, completion: { response in
                
                switch response {
                case let .Success(response: json):
                    if let comment = Comment.initfromDict(dict: json) {
                        fullfill(comment)
                    } else {
                        reject(ResponseError.withMessage("No such comment"))
                    }
                case let .Error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            })
        })
    }
    
    func fetchTopUsers(count: Int) -> Promise<[User]> {
        
        let urlString = APIKeys.users()
        
        return Promise.init(resolvers: { (fullfill, reject) in
            
            APIManager.shared.fetchItems(withUrl: urlString, completion: { (response) in
                switch response {
                case let .Success(response: json):
                    let users = Array(json.flatMap{ User.initfromDict(dict: $0 ) }.prefix(count))
                    fullfill(users)
                case let .Error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            })
        })
    }

    func fetchPhoto(number: Int) -> Promise<Photo> {
        
        let urlString = APIKeys.photo(n: number)
        
        return Promise.init(resolvers: { (fullfill, reject) in
            
            APIManager.shared.fetchItem(withUrl: urlString, completion: { (response) in
                switch response {
                case let .Success(response: json):
                    if let photo = Photo.initfromDict(dict: json) {
                        fullfill(photo)
                    } else {
                        reject(ResponseError.withMessage("No such photo"))
                    }
                case let .Error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            })
        })
    }
    
    func fetchTodo(number: UInt32) -> Promise<Todo> {
        
        let urlString = APIKeys.todo(n: number)
        
        return Promise.init(resolvers: { (fullfill, reject) in
            
            APIManager.shared.fetchItem(withUrl: urlString, completion: { (response) in
                switch response {
                case let .Success(response: json):
                    if let todo = Todo.initfromDict(dict: json) {
                        fullfill(todo)
                    } else {
                        reject(ResponseError.withMessage("No such todo"))
                    }
                case let .Error(message: msg):
                    reject(ResponseError.withMessage(msg))
                }
            })
        })
    }
}
