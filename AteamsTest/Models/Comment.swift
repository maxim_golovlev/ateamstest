//
//  Comment.swift
//  AteamsTest
//
//  Created by Admin on 19.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct Comment {
    
    var postId: Int
    var id: Int
    var name: String
    var email: String
    var body: String
    
    static func initfromDict(dict: [String: Any]?) -> Comment? {
        
        guard let dict = dict,
            let postId = dict["postId"] as? Int,
            let id = dict["id"] as? Int,
            let name = dict["name"] as? String,
            let email = dict["email"] as? String,
            let body = dict["body"] as? String else { return nil }
        
        let comment = Comment.init(postId: postId, id: id, name: name, email: email, body: body)
        return comment
    }
}
