//
//  User.swift
//  AteamsTest
//
//  Created by Admin on 19.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct User {
    var id: Int
    var name: String
    var phone: String
    
    static func initfromDict(dict: [String: Any]?) -> User? {
        
        guard let dict = dict,
            let id = dict["id"] as? Int,
            let name = dict["name"] as? String,
            let phone = dict["phone"] as? String else { return nil }
        
        let user = User.init(id: id, name: name, phone: phone)
        return user
    }
}
