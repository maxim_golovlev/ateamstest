//
//  Todo.swift
//  AteamsTest
//
//  Created by Admin on 20.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct Todo {
    
    var title: String
    var completed: Bool
    
    static func initfromDict(dict: [String: Any]?) -> Todo? {
        
        guard let dict = dict,
            let title = dict["title"] as? String,
            let completed = dict["completed"] as? Bool else { return nil }
        
        let todo = Todo.init(title: title, completed: completed)
        return todo
    }
}
