//
//  Post.swift
//  AteamsTest
//
//  Created by Admin on 19.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct Post {
    
    let userId: Int
    let id: Int
    let title: String
    let body: String
    
    static func initfromDict(dict: [String: Any]?) -> Post? {
        
        guard let dict = dict,
            let userId = dict["userId"] as? Int,
            let id = dict["id"] as? Int,
            let title = dict["title"] as? String,
            let body = dict["body"] as? String else { return nil }
            
        let post = Post.init(userId: userId, id: id, title: title, body: body)
        return post
    }
}
