//
//  Photo.swift
//  AteamsTest
//
//  Created by Admin on 19.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct Photo {
    var url: String
    
    static func initfromDict(dict: [String: Any]?) -> Photo? {
        
        guard let dict = dict,
            let url = dict["url"] as? String else { return nil }
        
        let photo = Photo.init(url: url)
        return photo
    }
    
}
