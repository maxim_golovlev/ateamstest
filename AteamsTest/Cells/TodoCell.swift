//
//  TodoCell.swift
//  AteamsTest
//
//  Created by Admin on 20.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class TodoCell: UITableViewCell, ConfigurableCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var completed: UILabel!
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    static var estimatedHeight: CGFloat? {
        return 44
    }
    
    func configure(with data:(title: String?, completed: Bool?)) {
        
        self.title.text = data.title
        let status = data.completed == true ? "yes" : "no"
        self.completed.text = "Completed: \(status)"
        
    }
    
    
}
