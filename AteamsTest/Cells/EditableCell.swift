//
//  PostCell.swift
//  AteamsTest
//
//  Created by Admin on 19.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

enum CellType {
    case Comment
    case Post
}

class EditableCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var textField: UITextField! {
        didSet {
            textField.text = "\(1)"
        }
    }
    
    var type: CellType?
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    static var estimatedHeight: CGFloat? {
        return 44
    }
    
    func configure(with data:(title: String?, subtitle: String?, type: CellType?)) {
        
        self.title.text = data.title
        self.subtitle.text = data.subtitle
        self.type = data.type
    }
    @IBAction func updateCell(_ sender: Any) {
        
        let key = type == .Comment ? AppActions.updateCommentCell : AppActions.updatePostCell
        TableCellAction.init(key: key, sender: self).invoke()
    }
}
