//
//  UserCell.swift
//  AteamsTest
//
//  Created by Admin on 20.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class UserCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    static var defaultHeight: CGFloat? {
        return UITableViewAutomaticDimension
    }
    
    static var estimatedHeight: CGFloat? {
        return 44
    }
    
    func configure(with data:(number: String?, name: String?)) {
        self.numberLabel.text = data.number
        self.nameLabel.text = data.name
    }
    
}
