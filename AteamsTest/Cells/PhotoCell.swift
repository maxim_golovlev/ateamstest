//
//  PhotoCell.swift
//  AteamsTest
//
//  Created by Admin on 20.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit
import SDWebImage

class PhotoCell: UITableViewCell, ConfigurableCell {
    
    @IBOutlet weak var photoView: UIImageView!
    
    static var defaultHeight: CGFloat? {
        return 200
    }
    
    func configure(with urlString: String?) {
        if let urlString = urlString, let url = URL.init(string: urlString) {
            photoView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "image_placeholder"))
        }
    }
}
